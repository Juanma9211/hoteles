<?php

use App\Http\Controllers\CalificacionController;
use App\Http\Controllers\HotelController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('hotel')->group(function () {
    Route::post('create', [HotelController::class, 'store']);
    Route::post('update/{hotelID}', [HotelController::class, 'update']);
    Route::post('verTodos', [HotelController::class, 'verTodos']);
    Route::post('verCatCal/{categoria}/{calificacion}', [HotelController::class, 'verCatCal']);
    Route::post('verXprecio/{true}', [HotelController::class, 'verXprecio']);
    Route::post('delete/{hotelID}', [HotelController::class, 'destroy']);
    Route::post('calificacion', [CalificacionController::class, 'store']);
});