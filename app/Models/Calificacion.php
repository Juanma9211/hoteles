<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Calificacion extends Model
{
    use HasFactory;
    
    protected $fillable = [
        'clientID',
        'hotelID',
        'calificacion',
        'comentario',
    ];

    public function crear($request){

        if(!$request->clientID){
            return ['mensaje' => 'falta el clientID', 'status' => 400];
        }else if(!$request->hotelID){
            return ['mensaje' => 'falta el hotelID', 'status' => 400];
        }else if(!$request->calificacion){
            return ['mensaje' => 'falta la calificacion', 'status' => 400];
        }else if($request->calificacion > 5){
            return ['mensaje' => 'La calificacion maxima es 5', 'status' => 400];
        }

        $this->fill($request->all());

        $this->save();

        return ['mensaje' => 'Registro exitoso', 'status' => 200];
    }
}
