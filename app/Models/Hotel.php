<?php

namespace App\Models;

use App\Http\Controllers\ImageController;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Models\Calificacion;

class Hotel extends Model
{
    use HasFactory;

    protected $fillable = [
        'hotelID',
        'hotelName',
        'categoria',
        'precio',
    ];

    public function calificacion()
    {
        return $this->hasMany(Calificacion::class, 'hotelID', 'hotelID');
    }

    public function scopeJoinCalificacion($query)
    {
        return $query->leftJoin('calificacions', 'hotels.hotelID', '=', 'calificacions.hotelID');
    }

    public function scopeFindCalificacion($query, $search)
    {
        return $query->where('calificacions.calificacion', $search);
    }
    
    public function scopeFindCategoria($query, $search)
    {
        return $query->orWhere('hotels.categoria', $search);
    }

    public function scopeSelectAll($query)
    {
        return $query->addSelect("hotels.*");
    }

    public function crear($request){

        if(!$request->hotelID){
            return ['mensaje' => 'falta el hotelID', 'status' => 400];
        }else if(!$request->hotelName){
            return ['mensaje' => 'falta el hotelName', 'status' => 400];
        }else if(!$request->categoria){
            return ['mensaje' => 'falta el categoria', 'status' => 400];
        }else if(!$request->precio){
            return ['mensaje' => 'falta el precio', 'status' => 400];
        }else if($request->categoria > 5){
            return ['mensaje' => 'La categoria maxima es 5', 'status' => 400];
        }

        $validation = Hotel::where('hotelID', $request->hotelID)->get();

        if ($validation != '[]') {
            return ['mensaje' => 'El codigo del hotel ya fue registrado', 'status' => 400];
        }

        $this->fill($request->all());

        $this->save();

        $imagen = new ImageController ();

        if($request->fotoHotel1)
        {
            $ruta = $imagen->saveImage($request, 'fotoHotel1');
            $this->where('hotelID', $this->hotelID)->update([
                'fotoHotel1' => $ruta
            ]);
        }

        if($request->fotoHotel2)
        {
            $ruta = $imagen->saveImage($request, 'fotoHotel2');
            $this->where('hotelID', $this->hotelID)->update([
                'fotoHotel2' => $ruta
            ]);
        }

        if($request->fotoHotel3)
        {
            $ruta = $imagen->saveImage($request, 'fotoHotel3');
            $this->where('hotelID', $this->hotelID)->update([
                'fotoHotel3' => $ruta
            ]);
        }

        return ['mensaje' => 'Registro exitoso', 'status' => 200];
    }

    public function actualizar($request){

        if($request->categoria > 5){
            return ['mensaje' => 'La categoria maxima es 5', 'status' => 400];
        }

        $this->fill($request->all());

        $this->save();

        $imagen = new ImageController ();

        if($request->fotoHotel1)
        {
            $imagen->borrarArchivo($this->fotoHotel1);
            $ruta = $imagen->saveImage($request, 'fotoHotel1');
            $this->where('hotelID', $this->hotelID)->update([
                'fotoHotel1' => $ruta
            ]);
        }

        if($request->fotoHotel2)
        {
            $imagen->borrarArchivo($this->fotoHotel2);
            $ruta = $imagen->saveImage($request, 'fotoHotel2');
            $this->where('hotelID', $this->hotelID)->update([
                'fotoHotel2' => $ruta
            ]);
        }

        if($request->fotoHotel3)
        {
            $imagen->borrarArchivo($this->fotoHotel3);
            $ruta = $imagen->saveImage($request, 'fotoHotel3');
            $this->where('hotelID', $this->hotelID)->update([
                'fotoHotel3' => $ruta
            ]);
        }

        return ['mensaje' => 'Actualizacion exitosa', 'status' => 200];
    }

}
