<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ImageController extends Controller
{
    public static function saveImage(Request $request, $nameFile){
        
        $file = $request->file($nameFile);

        $newName = $file->getClientOriginalName();
        
        //$img = Image::make($file->getRealPath())->resize('1000', '800');
        
        $newName= explode(".", $newName);
        $format = end($newName);
        $micro = str_replace(" ", "_", microtime());
        $micro = str_replace(".", "_", $micro);
        $newName = $micro.'.'.$format;
        
        //$img->response($format, 72);

        $routeFile = '/hoteles/images/';

        Storage::disk('public')->put($routeFile.$newName, \File::get($file));
        //Storage::disk($disk)->put($routeFile.$newName, $img);

        return '/storage'.$routeFile.$newName;
    }

    public static function borrarArchivo($rutaArchivo){
        $rutaArchivo = str_replace('/storage', '', $rutaArchivo);
        return Storage::disk('public')->delete($rutaArchivo);
    }
}
