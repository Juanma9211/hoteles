<?php

namespace App\Http\Controllers;

use App\Models\Hotel;
use Illuminate\Http\Request;

class HotelController extends Controller
{
    /**
     * Crea un nuevo hotel.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $r = (new Hotel)->crear($request);

        return response()->json($r['mensaje'], $r['status']);
    }

    /**
     * Muestra todos los hoteles.
     *
     * @return \Illuminate\Http\Response
     */
    public function verTodos()
    {
        return response()->json(Hotel::all(), 200);
    }

    /**
     * Muestra el resultado de la busqueda por categoria o clasificacion.
     *
     * @param  int  $categoria
     * @param  int  $califiacion
     * @return \Illuminate\Http\Response
     */
    public function verCatCal($categoria, $calificacion)
    {
        $r = Hotel::selectAll()
                    ->joinCalificacion()
                    ->findCalificacion($calificacion)
                    ->findCategoria($categoria)
                    ->with('calificacion')
                    ->get();

        return response()->json($r, 200);
    }
    /**
     * Muestra el resultado de la busqueda por precio, si mayor es
     * 1, ordena de mayor a menor; pero si es
     * 0, ordena de menor a mayor 
     * @param  int  $mayor
     * @return \Illuminate\Http\Response
     */
    public function verXprecio($mayor)
    {
        $order = $mayor == 1 ? 'desc' : 'asc';

        $r = Hotel::orderby('precio', $order)
            ->get();

        return response()->json($r, 200);
    }

    /**
     * Actualiza los datos de un hotel especifico.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $hotel = Hotel::where('hotelID', $id)->first();

        if (!$hotel) return response()->json(['mensaje' => 'Registro a editar no encontrado'], 400);

        $r = $hotel->actualizar($request);

        return response()->json($r['mensaje'], $r['status']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $hotel = Hotel::where('hotelID', $id)->first();

        if (!$hotel) return response()->json(['mensaje' => 'Registro a borrar no encontrado'], 400);

        $imagen = new ImageController ();

        $imagen->borrarArchivo($hotel->fotoHotel1);
        $imagen->borrarArchivo($hotel->fotoHotel2);
        $imagen->borrarArchivo($hotel->fotoHotel3);

        $hotel->delete();

        return response()->json(['mensaje' => 'Registro borrado exitosamente'], 200);
    }
}
