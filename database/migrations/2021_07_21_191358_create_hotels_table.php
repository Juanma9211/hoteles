<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHotelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hotels', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('hotelID');
            $table->string('hotelName');
            $table->smallInteger('categoria');
            $table->decimal('precio',12, 2);
            $table->string('fotoHotel1', 100)->nullable();
            $table->string('fotoHotel2', 100)->nullable();
            $table->string('fotoHotel3', 100)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hotels');
    }
}
